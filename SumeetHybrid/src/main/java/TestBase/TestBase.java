package TestBase;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ShootingStrategy;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;


public class TestBase {

	/***** Properties file*******/
	/*****Browser Initialization*******/
	/***** Screenshot *******/



	public static WebDriver driver = null;
	public static Properties config = null;
	//public static WebDriver driver = null;


	public static void init() throws IOException{
		loadProperties();
	}

	/***** Properties file
	 * @throws IOException *******/
	public static void loadProperties() throws IOException {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Config\\config.properties");		                          
		config = new Properties();
		config.load(fis);
		fis.close();	
	}

	/*****Browser Initialization********/	
	@SuppressWarnings("deprecation")
	public static WebDriver getBrowser(String browser) {

		if(browser.equalsIgnoreCase("Chrome")) {			
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");			
			driver = new ChromeDriver(options);			
		}

		if(browser.equalsIgnoreCase("Mozilla")) {			
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\drivers\\\\\\\\geckodriver.exe");
			driver= new FirefoxDriver();
		}
		if(browser.equalsIgnoreCase("InternetExplorer")){			
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");		
			InternetExplorerOptions options = new InternetExplorerOptions();
			options.setCapability("ie.ensureCleanSession", true);
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, true);			
			driver = new InternetExplorerDriver(options);
		}
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		return driver;
	}

	/***** Screenshot 
	 * @throws IOException *******/
	public static ArrayList<String> ss_paths = new ArrayList<String>();

	public static void getScreenshot() throws IOException {		
		Calendar calender = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

		//*****Taking the screenshot
		//File image = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		Screenshot fpScreenshot = new AShot().shootingStrategy(new ViewportPastingStrategy(1000)).takeScreenshot(driver);		
		BufferedImage image = fpScreenshot.getImage();
		//ImageIO.write(fpScreenshot.getImage(),"PNG",new File("D:///FullPageScreenshot.png")); 				
		//File image = fpScreenshot.getScreenshotAs(OutputType.FILE);


		//Path where to copy the taken Screenshot and also the name	
		String actualImageName = "Screenshot_"+formatter.format(calender.getTime())+".png";		
		String path = System.getProperty("user.dir")+"\\test-output-extent\\screenshots\\"+actualImageName;
		ss_paths.add(path);
		File destFile = new File(path);

		//Final Copying the file to destination
		//FileUtils.copyFile(image, destFile);
		ImageIO.write(image, "PNG", destFile);

	}

    





}


