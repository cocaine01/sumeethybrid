package Test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import TestBase.TestBase;

public class Trial extends TestBase{
	
	WebDriver  driver = null;
	
	@Test
	public void start() throws IOException {
		
		init();
		driver =TestBase.getBrowser(config.getProperty("browser"));
		driver.get(config.getProperty("url"));
	    TestBase.getScreenshot();
	    driver.manage().window().maximize();
	}

}
